#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#define	MAX_PARTICLES	10000

#define checkImageWidth 64
#define checkImageHeight 64

typedef struct _Ociste {
	GLdouble	x;
	GLdouble	y;
	GLdouble	z;
} Ociste;

Ociste	ociste = { 0.0f, 0.0f, 2.0f };


typedef struct _Glediste {
	GLdouble	x;
	GLdouble	y;
	GLdouble	z;
} Glediste;

Glediste glediste = { 0.0f, 0.0f, 0.0f };

struct Image {
	unsigned long sizeX;
	unsigned long sizeY;
	char* data;
};
typedef struct Image Image;

GLuint width = 600, height = 600;

typedef struct
{
	bool	active;					// Active (Yes/No)
	float	life;					// Particle Life
	float	fade;					// Fade Speed
	float	r, g, b;
	float	x;						// X Position
	float	y;						// Y Position
	float	z;						// Z Position
	float	xv;						// X speed
	float	yv;						// Y speed
	float	zv;						// Z speed

}
particle;

particle particles[MAX_PARTICLES];

GLuint tex;

GLubyte checkImage[checkImageWidth][checkImageHeight][3];

void myDisplay();
void myReshape(int width, int height);
void myRenderScene();
void idle();
void processNormalKeys(unsigned char key, int x, int y);
void makeCheckImage(void);
int ImageLoad(const char* filename, Image* image);
Image* loadTexture();
void myinit(void);

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Sustav cestica");
	myinit();

	//glEnable(GL_DEPTH_TEST);

	glutDisplayFunc(myDisplay);
	glutReshapeFunc(myReshape);
	glutIdleFunc(idle);
	glutKeyboardFunc(processNormalKeys);

	glutMainLoop();
	return 0;
}

void makeCheckImage(void)
{
	int i, j, c;
	for (i = 0; i < checkImageWidth; i++) {
		for (j = 0; j < checkImageHeight; j++) {
			c = ((((i & 0x8) == 0) ^ ((j & 0x8) == 0))) * 255;
			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
		}
	}
}

int ImageLoad(const char* filename, Image* image)
{
	FILE* file;
	unsigned long size; // size of the image in bytes.
	unsigned long i; // standard counter.
	unsigned short int planes; // number of planes in image (must be 1)
	unsigned short int bpp; // number of bits per pixel (must be 24)
	char temp; // temporary color storage for bgr-rgb conversion.
	// make sure the file is there.
	if ((file = fopen(filename, "rb")) == NULL) {
		printf("File Not Found : %s\n", filename);
		return 0;
	}
	// seek through the bmp header, up to the width/height:
	fseek(file, 18, SEEK_CUR);
	// read the width
	if ((i = fread(&image->sizeX, 4, 1, file)) != 1) {
		printf("Error reading width from %s.\n", filename);
		return 0;
	}
	//printf("Width of %s: %lu\n", filename, image->sizeX);
	// read the height
	if ((i = fread(&image->sizeY, 4, 1, file)) != 1) {
		printf("Error reading height from %s.\n", filename);
		return 0;
	}
	//printf("Height of %s: %lu\n", filename, image->sizeY);
	// calculate the size (assuming 24 bits or 3 bytes per pixel).
	size = image->sizeX * image->sizeY * 3;
	// read the planes
	if ((fread(&planes, 2, 1, file)) != 1) {
		printf("Error reading planes from %s.\n", filename);
		return 0;
	}
	if (planes != 1) {
		printf("Planes from %s is not 1: %u\n", filename, planes);
		return 0;
	}
	// read the bitsperpixel
	if ((i = fread(&bpp, 2, 1, file)) != 1) {
		printf("Error reading bpp from %s.\n", filename);
		return 0;
	}
	if (bpp != 24) {
		printf("Bpp from %s is not 24: %u\n", filename, bpp);
		return 0;
	}
	// seek past the rest of the bitmap header.
	fseek(file, 24, SEEK_CUR);
	// read the data.
	image->data = (char*)malloc(size);
	if (image->data == NULL) {
		printf("Error allocating memory for color-corrected image data");
		return 0;
	}
	if ((i = fread(image->data, size, 1, file)) != 1) {
		printf("Error reading image data from %s.\n", filename);
		return 0;
	}
	for (i = 0; i < size; i += 3) { // reverse all of the colors. (bgr -> rgb)
		temp = image->data[i];
		image->data[i] = image->data[i + 2];
		image->data[i + 2] = temp;
	}
	// we're done.
	return 1;
}

Image* loadTexture()
{
	Image* image1;
	// allocate space for texture
	image1 = (Image*)malloc(sizeof(Image));
	if (image1 == NULL) {
		printf("Error allocating space for image");
		exit(0);
	}
	const char* ime = "cestica.bmp";
	if (!ImageLoad(ime, image1)) {
		exit(1);
	}
	return image1;
}

void myinit(void)
{
	Image* image1 = loadTexture();
	if (image1 == NULL) {
		printf("Error while loading image");
		exit(0);
	}

	glBindTexture(GL_TEXTURE_2D, tex);

	//texture setup
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, image1->sizeX, image1->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, image1->data);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, tex);

	//init particles
	for (int i = 0; i < MAX_PARTICLES; i++)
	{
		particles[i].active = true;
		particles[i].life = 1.0f;
		particles[i].fade = float(rand() % 100) / 100.0f + 0.005f;
		particles[i].r = 1.0f;
		particles[i].g = 1.0f;
		particles[i].b = 1.0f;
		particles[i].xv = float((rand() % 60) - 20.0f);
		particles[i].yv = float((rand() % 60) - 20.0f);
		particles[i].zv = float((rand() % 60) - 20.0f);
		particles[i].x = 0.0f;
		particles[i].y = 0.0f;
		particles[i].z = 0.0f;
	}
}

void myDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(ociste.x, ociste.y, ociste.z, glediste.x, glediste.y, glediste.z, 0.0, 1.0, 0.0);
	myRenderScene();
	glutSwapBuffers();
}

void myReshape(int w, int h)
{
	width = w; height = h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-4.0,4.0,-4.0,4.0,-4.0,4.0);
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void myRenderScene()
{
	//glScalef(0.3f, 0.3f, 0.3f);
	glPushMatrix();
	for (int i = 0; i < MAX_PARTICLES; i++)
	{
		if (particles[i].active)
		{
			glColor4f(particles[i].r, particles[i].g, particles[i].b, 1.0);

			float x = particles[i].x;
			float y = particles[i].y;
			float z = particles[i].z;

			glBegin(GL_QUADS);
			glTexCoord2f(0.0, 0.0); glVertex3f(x - 0.1f, y - 0.1f, z);
			glTexCoord2f(1.0, 0.0); glVertex3f(x + 0.1f, y - 0.1f, z);
			glTexCoord2f(1.0, 1.0); glVertex3f(x + 0.1f, y + 0.1f, z);
			glTexCoord2f(0.0, 1.0); glVertex3f(x - 0.1f, y + 0.1f, z);
			glEnd();

			particles[i].x += particles[i].xv / (1000);
			particles[i].y += particles[i].yv / (1000); //+0.1f
			particles[i].z += particles[i].zv / (1000);

			particles[i].life -= particles[i].fade;

			particles[i].r = 0.0f;
			particles[i].g = 0.0f;
			particles[i].b = 0.0f;

			//when particle life reach 0.0 init it again
			if (particles[i].life < 0.0f)
			{
				particles[i].life = 1.0f;
				particles[i].fade = float(rand() % 100) / 100.0f + 0.005f;
				particles[i].xv = float((rand() % 60) - 30.0f);
				particles[i].yv = float((rand() % 60) - 30.0f);
				particles[i].zv = float((rand() % 60) - 30.0f);
				particles[i].x = 0.0f;
				particles[i].y = 0.0f;
				particles[i].z = 0.0f;
				particles[i].b = 1.0f;
			}
			// particle change color depending on part of life
			if (particles[i].life > 0.6f)
			{
				particles[i].b = 1.0f;
			}
			else if (particles[i].life > 0.4f && particles[i].life <= 0.6f)
			{
				particles[i].g = 1.0f;
			}
			else
			{
				particles[i].r = 1.0f;
			}
		}
	}
	glPopMatrix();
}

void processNormalKeys(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'l': ociste.x = ociste.x + 0.1f;
		break;

	case 'j': ociste.x = ociste.x - 0.1f;
		break;

	case 'i': ociste.y = ociste.y + 0.1f;
		break;

	case 'k': ociste.y = ociste.y - 0.1f;
		break;

	case 'u': ociste.z = ociste.z + 0.1f;
		break;

	case 'o': ociste.z = ociste.z - 0.1f;
		break;
	case 'a': glediste.x = glediste.x + 0.1f;
		break;

	case 'd': glediste.x = glediste.x - 0.1f;
		break;

	case 'w': glediste.y = glediste.y + 0.1f;
		break;

	case 's': glediste.y = glediste.y - 0.1f;
		break;

	case 'q': glediste.z = glediste.z + 0.1f;
		break;

	case 'e': glediste.z = glediste.z - 0.1f;
		break;
	case 'r':
		ociste = { 0.0f, 0.0f, 2.0f };
		glediste = { 0.0f, 0.0f, 0.0f };
		break;

	case 27:  exit(0);
		break;
	}

	glutPostRedisplay();
}

void idle()
{
	Sleep(30);
	glutPostRedisplay();
}

#include <GL/glut.h>
#include <cmath>
#include <stdio.h>
#include <glm/gtx/string_cast.hpp>
#include <glm/glm.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

double t = 0;
int numOnMatrix = 0;
int numOfPoints = 0;

class Camera {
	double theta;      // determines the x and z positions
	double y;          // the current y position
	double dTheta;     // increment in theta for swinging the camera around
	double dy;         // increment in y for moving the camera up/down
public:
	Camera() : theta(0), y(4), dTheta(0.04), dy(0.2) {}
	double getX() { return 10 * cos(theta); }
	double getY() { return y; }
	double getZ() { return 10 * sin(theta); }
	void moveRight() { theta += dTheta; }
	void moveLeft() { theta -= dTheta; }
	void moveUp() { y += dy; }
	void moveDown() { if (y > dy) y -= dy; }
};

Camera camera;
bool animation = true;
double matrixPoints[100][3];

glm::mat4 matricaB = glm::mat4((double)-1 / 6, (double)3 / 6, (double)-3 / 6, (double)1 / 6,
(double)3 / 6, (double)-6 / 6, (double)0 / 6, (double)4 / 6, (double)-3 / 6, (double)3 / 6,
(double)3 / 6, (double)1 / 6, (double)1 / 6, (double)0 / 6, (double)0 / 6, (double)0 / 6);

glm::vec3 s = glm::vec3((double)0, (double)1, (double)0); //pocetna orjentacija proizvoljna

glm::vec4 translateVector1;
glm::vec4 translateVector2;

void translateRotate() {
	if (animation) {
		glm::vec4 vecT = glm::vec4(t * t * t, t * t, t, 1);

		glm::mat4 matrixPoints4 = { matrixPoints[numOnMatrix][0], matrixPoints[numOnMatrix + 1][0], matrixPoints[numOnMatrix + 2][0], matrixPoints[numOnMatrix + 3][0],
			matrixPoints[numOnMatrix][1], matrixPoints[numOnMatrix + 1][1], matrixPoints[numOnMatrix + 2][1], matrixPoints[numOnMatrix + 3][1],
			matrixPoints[numOnMatrix][2], matrixPoints[numOnMatrix + 1][2], matrixPoints[numOnMatrix + 2][2], matrixPoints[numOnMatrix + 3][2],
			1, 1, 1, 1 };

		glm::vec4 result = vecT * matricaB * matrixPoints4;

		glm::vec4 vecTT = glm::vec4(t * t * 3, t * 2, 1, 0);

		glm::vec4 resultT = vecTT * matricaB * matrixPoints4;

		glTranslated(result.x, result.y, result.z);

		double angle;
		angle = acos((s.x * resultT.x + s.y * resultT.y + s.z * resultT.z)
			/ ( sqrt(pow(s.x,2) + pow(s.y, 2) + pow(s.z, 2)) * sqrt(pow(resultT.x, 2) + pow(resultT.y, 2) + pow(resultT.z, 2)) ) )
			* (180.0 / 3.141592653589793238463);
		glRotated( angle, (s.y * resultT.z) - (resultT.y * s.z), (s.z * resultT.x) - (resultT.z * s.x), (s.x * resultT.y) - (resultT.x * s.y));
		
		translateVector2 = resultT;
		translateVector1 = result;
	}
	else glTranslated(0, 0, 0);
		
}

void drawTriangle() {
	glBegin(GL_TRIANGLES);
	glColor3f(0, 1, 0);	glVertex3d(0.0, 0.0, 0.0);
	glColor3f(0, 0, 0);	glVertex3d(0.0, 2.0, 0.0);
	glColor3f(0, 0.5, 0);	glVertex3d(0.0, 1.0, 1.0);
	glEnd();
}

void drawAllPoints() {
	glBegin(GL_LINES);
	glColor3f(1.0, 1.0, 1.0);
	for (int i = 0; i < numOfPoints - 1; i++)
	{
		glVertex3f(matrixPoints[i][0], matrixPoints[i][1], matrixPoints[i][2]);
		glVertex3f(matrixPoints[i + 1][0], matrixPoints[i + 1][1], matrixPoints[i + 1][2]);
	}
	glEnd();
}

void drawTangent() {
	glTranslated(translateVector1.x, translateVector1.y, translateVector1.z);
	glBegin(GL_LINES);
	glColor3f(0, 1, 0);
	glVertex3d(0, 0, 0);
	//glVertex3d((translateVector2.x - translateVector1.x) * 0.5, (translateVector2.y - translateVector1.y) * 0.5, (translateVector2.z - translateVector1.z) * 0.5);
	glVertex3d((translateVector2.x) * 0.5, (translateVector2.y) * 0.5, (translateVector2.z) * 0.5);

	glEnd();
}

void drawGround() {
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
	glNormal3d(0, 1, 0);
	for (int x = -10; x < 10 - 1; x++) {
		for (int z = -10; z < 10 - 1; z++) {
			if ((x + z) % 2 == 0) {
				glColor3f(1.0, 1.0, 1.0);
			}
			else glColor3f(0.0, 0.0, 0.0);
			glVertex3d(x, 0, z);
			glVertex3d(x + 1, 0, z);
			glVertex3d(x + 1, 0, z + 1);
			glVertex3d(x, 0, z + 1);
		}
	}
	glEnd();
}

void drawBSpline() {
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 1.0);
	glm::vec4 lastResult;
	bool setFirst = false;
	for (int i = 0; i < numOfPoints-3; i++) {
		for (double j = 0; j < 1; j+=0.1)
		{
			glm::vec4 vecT = glm::vec4(j * j * j, j * j, j, 1);

			glm::mat4 matrixPoints4 = { matrixPoints[i][0], matrixPoints[i+1][0], matrixPoints[i+2][0], matrixPoints[i + 3][0],
										matrixPoints[i][1], matrixPoints[i + 1][1], matrixPoints[i + 2][1], matrixPoints[i + 3][1],
										matrixPoints[i][2], matrixPoints[i + 1][2], matrixPoints[i + 2][2], matrixPoints[i + 3][2],
										1, 1, 1, 1};

			glm::vec4 result = vecT * matricaB * matrixPoints4;

			if (setFirst) {
				glVertex3f(lastResult.x, lastResult.y, lastResult.z);
				glVertex3f(result.x, result.y, result.z);
			}
			else setFirst = true;

			lastResult = result;
		}
	}
	glEnd();
}

void processSpecialKeys(int key, int xx, int yy) {
	switch (key) {
		case GLUT_KEY_LEFT: camera.moveLeft(); break;
		case GLUT_KEY_RIGHT: camera.moveRight(); break;
		case GLUT_KEY_UP: camera.moveUp(); break;
		case GLUT_KEY_DOWN: camera.moveDown(); break;
	}
	glutPostRedisplay();
}

void renderScene(void) {
	if (t >= 1) {
		t = 0;
		numOnMatrix++;
	}
	if (numOnMatrix == 9) {
		animation = false;
	}
	// Clear Color and Depth Buffers
	glClearColor(0.8, 0.8, 1, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();
	// Set the camera
	gluLookAt(camera.getX(), camera.getY(), camera.getZ(),
		0, 0.0, 0,
		0.0, 1.0, 0.0);

	// Draw ground
	drawGround();

	glPushMatrix();
	translateRotate();
	drawTriangle();
	glPopMatrix();

	glPushMatrix();
	drawTangent();
	glPopMatrix();

	t += (double)0.001;

	glPushMatrix();
	drawBSpline();
	glPopMatrix();

	// draw all points from matrix and connect it with lines
	glPushMatrix();
	drawAllPoints();
	glPopMatrix();

	glutSwapBuffers();
}

void changeSize(GLint w, GLint h) {
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, GLfloat(w) / GLfloat(h), 1.0, 1000.0);
	glMatrixMode(GL_MODELVIEW);
}

void processNormalKeys(unsigned char key, int x, int y) {
	if (key == 27)
		exit(0);
}

// Requests to draw the next frame.
void timer(int v) {
	if (animation) {
		glutPostRedisplay();
	}
	glutTimerFunc(1000 / 60, timer, v);
}

int main(int argc, char** argv) {
	string line;
	ifstream myfile("coordinates.txt");
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			string num = "";
			int i = 0;
			for (auto x : line)
			{
				if (x == ' ')
				{
					matrixPoints[numOfPoints][i] = stod(num);
					i++;
					num = "";
				}
				else {
					num = num + x;
				}
			}
			matrixPoints[numOfPoints][i] = stod(num);
			i = 0;
			numOfPoints++;
		}
		myfile.close();
	}

	else cout << "Unable to open file";

	// init GLUT and create window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Bspline animacija");

	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);

	glutTimerFunc(100, timer, 0);

	// OpenGL init
	glEnable(GL_DEPTH_TEST);

	// enter GLUT event processing cycle
	glutMainLoop();

	return 1;
}
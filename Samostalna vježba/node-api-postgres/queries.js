const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'animacija',
  password: 'your password',
  port: 5432,
})

const getQuestions = (request, response) => {
  const part = parseInt(request.params.part)
  const num = parseInt(request.params.num)
  pool.query('SELECT * FROM questions WHERE part = $1 ORDER BY random() LIMIT $2', [part,num], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

module.exports = {
  getQuestions
}

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000
const db = require('./queries')
var cors = require('cors')

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)

var corsOptions = {
  origin: true,
  optionsSuccessStatus: 200
}

app.get('/', cors(corsOptions), (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

app.get('/questions/:part/:num', cors(corsOptions), db.getQuestions)

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})